package com.preeya.week11;


public class Dog extends Animal implements Walkable {

    public Dog(String name, int numberOfLeg) {
        super(name, 4);
        
    }
    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
        
    }

    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }
    
    @Override
    public void walk() {
        System.out.println(this.toString() + " walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString() + " run.");
        
    }
}