package com.preeya.week11;

public interface Walkable {

    public void run();
    public void walk();
}