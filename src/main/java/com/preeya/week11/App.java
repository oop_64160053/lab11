package com.preeya.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println(":Bat:");
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.takeoff();
        bat1.landing();
        bat1.walk();
        System.out.println(":Fish:");
        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        System.out.println(":Plane:");
        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.landing();
        System.out.println(":Crocodile:");
        Crocodile crocodile1 = new Crocodile("Co", 4);
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        System.out.println(":Snake:");
        Snake snake1 = new Snake("Nake");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();
        System.out.println(":Submarine:");
        Submarine submarine1 = new Submarine("Sub", "Sub Engine");
        submarine1.swim();
        System.out.println(":Bird:");
        Bird bird1 = new Bird("Noke", 2);
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.landing();
        bird1.fly();
        bird1.walk();
        System.out.println(":Cat:");
        Cat cat1 = new Cat("Meow",4);
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        System.out.println(":Dog:");
        Dog dog1 = new Dog("Siberian",4);
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        System.out.println(":Human:");
        Human Human1 = new Human("Tongnsk",2);
        Human1.eat();
        Human1.sleep();
        Human1.walk();
        Human1.run();
        System.out.println(":Rat:");
        Rat rat1 = new Rat("Jerry",4);
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        System.out.println();
        System.out.println(":Fly:");
        Flyable[] flyablesObject = { bat1, plane1 };
        for (int i = 0; i < flyablesObject.length; i++) {
            flyablesObject[i].fly();
            flyablesObject[i].landing();
            flyablesObject[i].takeoff();
        }
        System.out.println(":swim:");
        Swimable[] swimablesObject = { fish1, submarine1 , crocodile1 };
        for (int i = 0; i < swimablesObject.length; i++) {
            swimablesObject[i].swim();
            
        }
        System.out.println(":Crawl:");
        Crawlable[] crawlableObject = { snake1, crocodile1 };
        for (int i = 0; i < crawlableObject.length; i++) {
            crawlableObject[i].crawl();
            
        }
        System.out.println(":Walk:");
        Walkable[] walklableObject = { rat1 , Human1 , dog1 , cat1 , bird1 , bat1 };
        for (int i = 0; i < walklableObject.length; i++) {
            walklableObject[i].walk();
            walklableObject[i].run();
            
        }


    }
}
