package com.preeya.week11;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}
