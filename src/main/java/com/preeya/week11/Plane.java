package com.preeya.week11;

public class Plane extends Vahicle implements Flyable {

    public Plane(String name, String engineName) {
        super(name, engineName);
        
    
}

    @Override
    public void fly() {
        System.out.println(this.toString() + " fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString() + " landing.");
        
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + " takeoff.");
        
    }
    @Override
    public String toString() {
        return "Plane(" + this.getName() +")" + " engine: " + this.getEngineName();
    }
}
